package db

import (
	"context"
	"database/sql"
	"time"

	"log"

	_ "github.com/go-sql-driver/mysql"
	comm "gitlab.com/bomz/comm"
)

// database connection
var conn *sql.DB

func InitMainDatabase() {
	var err error
	log.Println(comm.GetMainDatabaseConnectInfo())
	conn, err = sql.Open("mysql", comm.GetMainDatabaseConnectInfo())
	if err != nil {
		log.Println("main database connect error.", err)
		panic(err)
	}

	conn.SetConnMaxLifetime(time.Duration(comm.GetConnMaxLifetime() * time.Minute.Nanoseconds()))
	conn.SetMaxOpenConns(comm.GetMaxOpenConns())
	conn.SetMaxIdleConns(comm.GetMaxIdleConns())

	log.Println("main database connect success.")
}

// database session open
func OpenSession() (*sql.Conn, error) {
	return conn.Conn(context.Background())
}
