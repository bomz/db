package db

import (
	"encoding/json"
	"net/http"
	"time"

	"log"

	"entity"

	"github.com/go-redis/redis"
	comm "gitlab.com/bomz/comm"
)

var sessionConn *redis.Client

// 사용자 세션 자동 로그아웃 시간 (기본 1시간 설정)
var sessionTimeout time.Duration = time.Hour

/**	login check cookie name	*/
const CookieName = "bomzck"

func InitSessionDatabase() {
	sessionConn = redis.NewClient(&redis.Options{
		Addr:     comm.GetSessionDatabaseAddr(),
		Password: comm.GetSessionDatabasePassword(),
		DB:       0,
	})

	// 설정파일에 있는 시간으로 자동 로그아웃 시간 변경
	sessionTimeout = comm.GetSessionTimeout()

	log.Println("session database connect success.")
}

func GetSessionValue(s *string) ([]byte, error) {
	return sessionConn.Get(*s).Bytes()
}

func SetSessionValue(s *string, jsn *[]byte) *redis.StatusCmd {
	return sessionConn.Set(*s, *jsn, sessionTimeout)
}

func DelSessionValue(s *string) (int64, error) {
	return sessionConn.Del(*s).Result()
}

// insert user session in session db
func SaveSession(res *http.ResponseWriter, emp *entity.Employee) error {
	jsn, err := json.Marshal(emp)
	if err != nil {
		return err
	}

	checkMaximumSessionCount(emp)

	s := emp.Email + comm.SessionIdDelimiter + comm.MakeRandomName()
	ck := NewCookie(&s)
	http.SetCookie(*res, ck)

	return SetSessionValue(&s, &jsn).Err()
}

// 동시 로그인 최대치 검사
func checkMaximumSessionCount(emp *entity.Employee) {
	// 로그인 요청아이디로 세션디비에 등록된 TTL 정보 검색
	v := sessionConn.Scan(0, emp.Email+comm.SessionIdDelimiter+"*", 99999999999999)
	if v.Err() != nil {
		// 에러가 발생해도 필수 처리 항목이 아니므로 처리하지 않는다
		return
	}

	r, _, e := v.Result()
	if e != nil {
		// 에러가 발생해도 필수 처리 항목이 아니므로 처리하지 않는다
		return
	}

	if len(r) < comm.GetSessionMaximum() {
		// 동시접속 최대 수를 넘지않았을 경우 처리하지 않는다
		return
	}

	delOldSession(&r) // 사용한지 가장 오래된 세션 로그아웃 처리
}

// 마지막 사용한지 제일 오래된 세션 로그아웃 처리
func delOldSession(r *[]string) {
	var lst float64 = -1 // LAST ACCESS TIME
	var key string
	for _, v := range *r {
		t := sessionConn.TTL(v)
		if t.Err() != nil {
			// 필수항목이 아니므로 별도의 예외처리가 없다
			return
		}

		d, e := t.Result()
		if e != nil {
			return
		}

		if d.Seconds() == -2 {
			// 데이터가 없을 경우 시간이 지나 사라진 세션정보이므로 nil 리턴
			return
		}

		if lst == -1 || lst > d.Seconds() {
			lst = d.Seconds()
			key = v
		}
	}

	if key != "" {
		DelSessionValue(&key) // logout
	}

}

func NewCookie(s *string) *http.Cookie {
	return &http.Cookie{
		Name:     CookieName,
		Value:    *s,
		MaxAge:   84600,
		Expires:  time.Now().AddDate(0, 0, 1),
		Secure:   false,
		HttpOnly: true,
	}
}
